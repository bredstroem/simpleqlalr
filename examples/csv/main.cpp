#include <QCoreApplication>
#include <QStringList>
#include <QDebug>
#include "csvparser.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QStringList input{"a,b,c\n1,2,3\n",
                      "a,b,c\n1,2,3",
                      "\"q\n\",1\n 3 ,",
                      "Σ,utf8 rules...\n  åäö,\n"};

    CsvParser parser;
    for (const QString & str : input) {
        CsvLexer lexer(str);
        qDebug() << "Input:" << endl << str;
        if (!parser.parse(lexer)) {
            qDebug() << parser.errorMessage();
        } else {
            qDebug() << "Result:" << parser.records();
        }
    }
}
