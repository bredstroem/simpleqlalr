------------------------------------------------------------------------------------
-- See definition of csv at http://tools.ietf.org/html/rfc4180
-- This is a bit stupid example because the fileformat is really designed to be
-- parsed without the need of a grammar but its a decent example that shows
-- how to implement a custom lexer.
------------------------------------------------------------------------------------
%parser CsvParser

%token SEPARATOR "separator"
%token LF "linefeed"
%token FIELD "field"

%start Root

/: // Begin declaration

#include <QStringList>
#include <QRegExp>
#include <QList>

class CsvLexer : public $lexer
{

public:
    // Note: We remove trailing endlines before we begin
    CsvLexer(const QString &string, QChar sep = ',') :
       $lexer(QString(string).replace(QRegExp("\n+$"),"")), m_sep(sep), m_done(false) {}
    // We need to implement our own lex() for this parser
    virtual int lex();

private:
    int parseEscaped();
    int parseNonEscaped();
    QChar m_sep;
    bool m_done;
};

class $parser : public $grammar
{
public:
    QList<QStringList> records() { return m_records; }
    virtual bool parse($lexer& lexer) {
        m_records.clear();
        m_field.clear();
        return $grammar::parse(lexer);
    }
private:
    virtual void switchRule(int r, $lexer& lexer);
    void deb(int r);
    QList<QStringList> m_records;
    QStringList m_field;
};
:/ -- end header definitions


/. // Begin implementation
int CsvLexer::lex() {
    clearSymbol();
    if (pos() < size()) {
        QChar c = current();
        if      (c == m_sep) { inc(); return Token::SEPARATOR; }
        else if (c == '\n')  { inc(); incLine(); return Token::LF; }
        else if (c == '"' )  { inc(); return parseEscaped(); }
        else                 { return parseNonEscaped(); }
    } else if (!m_done && pos() > 0) {
        m_done = true;
        QChar c = getData().at( pos() - 1 );
        if (c == m_sep) { setSymbol(""); return Token::FIELD; }
    }
    return Token::EOF_SYMBOL;
}

int CsvLexer::parseEscaped() {
    const QString& uc = getData();
    int len = size();
    QString symbol = "";
    while( pos() < len ) {
        QChar c = uc.at(pos());
        if (c == '"') {
            // Look for a second double quote or stop.
            inc();
            if( uc.at(pos()) != QLatin1Char('"') ) {
                setSymbol(symbol);
                return Token::FIELD;
            }
        }
        if (c == '\n') incLine(); // Only for the error message
        symbol += c;
        inc();
    }
    return -1; // error (EOF) we expect a final double quote
}

int CsvLexer::parseNonEscaped() {
    const QString& uc = getData();
    int len = uc.length();
    int start = pos();
    while( pos() < len ) {
        QChar c = uc.at(pos());
        if (c == m_sep) break;
        if(uc.at(pos()) == QLatin1Char('\n')) {
            if (pos() > 0 && uc.at(pos() - 1) == QLatin1Char('\r')) {
                // Windows style linefeed, skip the \r in the result symbol
                setSymbol( uc.mid( start, pos() - start - 1 ) );
                return Token::FIELD;
            }
            break;
        }
        inc();
    }
    setSymbol( uc.mid( start, pos() - start ) );
    return Token::FIELD;
}
./ -- end implementation


/r -- rules begin

Root::= Records;

Records::= ;
Records::= Record; /{ m_records << m_field; }/
Records::= Records LF Record; /{ if(m_field.length()) m_records << m_field; }/

Record::= FIELD; /{ m_field.clear(); m_field << symbol(1).toString(); }/
Record::= Record SEPARATOR Field; /{ m_field << symbol(3).toString(); }/
-- Record::= ; /{ m_field.clear(); }/

Field::= LF;
Field::= FIELD;
Field::= SEPARATOR; /{ m_field << ""; }/

/e -- rules end
