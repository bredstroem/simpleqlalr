#include <QCoreApplication>
#include <QDebug>
#include "jsonparser.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString str = R"({"data": [true,1,2,null,false,{"something ELSE": true},"ÅLLKÖ","Ελλάδα",1.4e-15]})";
    JsonLexer lexer(str);
    JsonParser parser;
    if (!parser.parse(lexer)) {
        qDebug() << parser.errorMessage();
    } else {
        qDebug() << parser.result();
    }
}
