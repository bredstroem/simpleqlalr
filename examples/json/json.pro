QT       += core
QT       -= gui

TARGET = json
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

SIMPLEQLALR_SOURCE = json.g
SIMPLEQLALR_RESULTS = jsonparser.cpp jsonparser.h

simpleqlalr.commands = ~/bin/simpleqlalr ${QMAKE_FILE_IN}
simpleqlalr.input = SIMPLEQLALR_SOURCE
simpleqlalr.name = simpleqlalr
simpleqlalr.output = $$SIMPLEQLALR_RESULTS
simpleqlalr.variable_out = SOURCES
simpleqlalr.CONFIG += target_predeps
QMAKE_EXTRA_COMPILERS += simpleqlalr

OTHER_FILES += $$SIMPLEQLALR_SOURCE
