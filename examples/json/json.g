%parser JsonParser

%token LCURLY "{"
%token RCURLY "}"
%token LSQUARE "["
%token RSQUARE "]"
%token COLON ":"
%token COMMA ","

%token FALSE "false"
%token TRUE "true"
%token TNULL "null"

%token NUMBER "number"
%token STRING "string"

%start Root

/:

#include <QVariantList>
#include <QVariantMap>

class JsonLexer : public $lexer
{

public:

    JsonLexer(const QString &string) : $lexer(string)
    {
          addToken(Token::LCURLY);
          addToken(Token::RCURLY);
          addToken(Token::LSQUARE);
          addToken(Token::RSQUARE);
          addToken(Token::COLON);
          addToken(Token::COMMA);
          addToken(Token::FALSE);
          addToken(Token::TRUE);
          addToken(Token::TNULL);
          enableNumberToken(Token::NUMBER);
          enableStringToken(Token::STRING, '"');
    }
};

class $parser : public $grammar
{
protected:
    virtual void switchRule(int r, $lexer &lexer);
    virtual int reallocateStack() {
        int size = $table::reallocateStack();
        m_map.resize(size);
        m_list.resize(size);
        return size;
    }

private:
    QVariantMap &map(int index) { return m_map[stackOffset()+index]; }
    QVariantList &list(int index) { return m_list[stackOffset()+index]; }
    QVector<QVariantMap> m_map;
    QVector<QVariantList> m_list;
};

:/

/r -- begin rules

Root ::= Value; /{ setResult(symbol(1)); }/

Object ::= LCURLY Members RCURLY;                /{ symbol(1) = map(2); }/
Members ::= STRING COLON Value;                  /{ map(1).clear(); map(1).insert(symbol(1).toString(), symbol(3)); }/
Members ::= Members COMMA STRING COLON Value;  /{ map(1).insert(symbol(3).toString(), symbol(5)); }/
Members ::= ;                                        /{ map(1).clear(); }/

Array ::= LSQUARE Values RSQUARE; /{ symbol(1) = list(2); }/
Values ::= Value;                     /{ list(1).clear(); list(1).append(symbol(1)); }/
Values ::= Values COMMA Value;      /{ list(1).append(symbol(3)); }/
Values ::= ;                          /{ list(1).clear(); }/

Value ::= Object;
Value ::= Array;
Value ::= TNULL;
Value ::= NUMBER;
Value ::= STRING;
Value ::= FALSE;   /{ symbol(1) = QVariant(false); }/
Value ::= TRUE;    /{ symbol(1) = QVariant(true); }/

/e -- end rules
