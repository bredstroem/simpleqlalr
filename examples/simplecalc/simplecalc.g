-------------------------------------------------------------------------------
-- The %parser directive is used to define the name of the generated parser,
-- which is the sublass where we implement our actions and grammar rules.
-- The name is also used for the output filenames (in lowercase), so
-- simplelalr will in this case create two files: the declaration
-- in calcparser.h and the implementation in calcparser.cpp.
-------------------------------------------------------------------------------
%parser CalcParser

-------------------------------------------------------------------------------
-- All tokens that we want to use in the grammar rules must be defined here.
-- The string value after the token name is used for user friendly error
-- messages and can also be used to configure the lexer with the
-- addToken() method.
-------------------------------------------------------------------------------

%token PLUS "+"
%token MINUS "-"
%token INT "int"

-------------------------------------------------------------------------------
-- We must define a start rule for the parser generator.
-------------------------------------------------------------------------------

%start Root

-------------------------------------------------------------------------------
-- Lexer and Parser
-- The content between /: ... :/ is put in the header, in our case
-- "calcparser.h" because of the %parser directive above.
-- The $lexer and the $grammar are replaced by the class names CalcParserLexer
-- and CalcParserGrammar.
-- ( implementation code can be inserted between /. ... ./ )
-------------------------------------------------------------------------------

/:
#include <QDebug>
class CalcLexer : public $lexer
{

public:

    CalcLexer(const QString &string) : $lexer(string)
    {
          // the PLUS token is defined explicitly with the char '+'
          // while the MINUS token is defined with the string from the token
          // list above. (to illustrate the options, not for any other reason.)
          addToken(Token::PLUS, '+');
          addToken(Token::MINUS);
          //
          enableNumberToken(Token::INT, false);
    }
};

class $parser : public $grammar
{
    // The parser must implement the following abstract method, which is
    // where the grammar rules are matched to actions (see below).
    virtual void switchRule(int r, $lexer& lexer);
};

:/

-------------------------------------------------------------------------------
-- Grammar rules and actions
--
-- The grammar rules are defined in the implementation of abstract method
-- switchRule() defined in the $grammar class.
-- The /r /e shortcuts simply expands to
-- "void $parser::switchRule(int r, $lexer& lexer) { switch(r) {"
-- and, "} }" respectively.
-- The shortcuts /{ ... }/ expands to "case $rule_number: { ... } break;"
-- The symbol method is a "QVariant& symbol(index)" method that returns the
-- symbol from the symbol stack reindexed to the current position (starting at
-- 1).
-- setResult(QVariant) sets the result obtained from result(), see main.cpp.
-------------------------------------------------------------------------------

/r -- rules

Root ::= Expr; /{ setResult(symbol(1)); }/

Expr ::= ;
Expr ::= INT ;
Expr ::= Expr MINUS INT ; /{ symbol(1) = symbol(1).toInt() - symbol(3).toInt(); }/
Expr ::= Expr PLUS INT ;  /{ symbol(1) = symbol(1).toInt() + symbol(3).toInt(); }/

/e -- end rules
