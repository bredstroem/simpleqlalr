#include <QStringList>
#include <QDebug>
#include "calcparser.h"

int main()
{
    QStringList input{"5+3",
                      "3-5",
                      "5+3 - 6+8-   12\n+2",
                      "1+2+ 3+ 4 +5",
                      "0",
                      "+5",
                      "",
                      "-1"};

    CalcParser parser;
    for (const QString & str : input) {
        CalcLexer lexer(str);
        if (!parser.parse(lexer)) {
            qDebug() << str << parser.errorMessage();
        } else {
            qDebug() << str << "=" << parser.result().toDouble();
        }
    }

    return 0;
}
