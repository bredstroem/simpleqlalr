QT       += core
QT       -= gui

TARGET = calc
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

SIMPLEQLALR_SOURCE = calc.g
SIMPLEQLALR_RESULTS = calcparser.cpp calcparser.h

simpleqlalr.commands = ~/bin/simpleqlalr --namespace ParserNameSpace ${QMAKE_FILE_IN}
simpleqlalr.input = SIMPLEQLALR_SOURCE
simpleqlalr.name = simpleqlalr
simpleqlalr.output = $$SIMPLEQLALR_RESULTS
simpleqlalr.variable_out = SOURCES
simpleqlalr.CONFIG += target_predeps
QMAKE_EXTRA_COMPILERS += simpleqlalr

OTHER_FILES += $$SIMPLEQLALR_SOURCE
