#include <QCoreApplication>
#include <QStringList>
#include <QDebug>
#include "calcparser.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QStringList input{"5+3",
                      "3-5",
                      "5+3 - 6+8-   12\n+2",
                      "1+2+ 3+ 4 +5",
                      "0",
                      "+5",
                      "",
                      "-1",
                      "3*5",
                      "9/3",
                      "(5+3)*8",
                      "4+12/4",
                      "(4+12)/4",
                      "3+3.5*2-2/(2-(2-1))"};

    ParserNameSpace::CalcParser parser;
    for (const QString & str : input) {
        ParserNameSpace::CalcLexer lexer(str);
        if (!parser.parse(lexer)) {
            qDebug() << str << parser.errorMessage();
        } else {
            qDebug() << str << "=" << parser.result().toDouble();
        }
    }
}
