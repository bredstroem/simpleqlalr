------------------------------------------------------------------------------------
-- A simple calculator for expressions that support +,-,*,/ and parenthesis,
-- such as (5+4)*3.14-12
------------------------------------------------------------------------------------

%parser CalcParser

%token PLUS "+"
%token MINUS "-"
%token MULT "*"
%token DIV "/"
%token LPAREN "("
%token RPAREN ")"
%token NUMBER "number"

%start Root

/:

class CalcLexer : public $lexer
{

public:

    CalcLexer(const QString &string) : $lexer(string)
    {
          addToken(Token::PLUS);
          addToken(Token::MINUS);
          addToken(Token::MULT);
          addToken(Token::DIV);
          addToken(Token::LPAREN);
          addToken(Token::RPAREN);
          enableNumberToken(Token::NUMBER);
    }
};

class $parser : public $grammar
{
    virtual void switchRule(int r, $lexer& lexer);
};

:/

/r -- begin rules

Root ::= Expr;  /{ setResult(symbol(1)); }/

Atom ::= NUMBER;
Atom ::= LPAREN Expr RPAREN;  /{ symbol(1) = symbol(2); }/

Mult ::= Atom;
Mult ::= Atom DIV Atom;  /{ symbol(1) = symbol(1).toDouble() / symbol(3).toDouble(); }/
Mult ::= Atom MULT Atom; /{ symbol(1) = symbol(1).toDouble() * symbol(3).toDouble(); }/

Expr ::= ;
Expr ::= Mult;
Expr ::= Expr PLUS Mult;  /{ symbol(1) = symbol(1).toDouble() + symbol(3).toDouble(); }/
Expr ::= Expr MINUS Mult; /{ symbol(1) = symbol(1).toDouble() - symbol(3).toDouble(); }/

/e -- end rules
