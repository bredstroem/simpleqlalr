#include <QCoreApplication>
#include <QStringList>
#include <QDebug>
#include "ocparser.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString fact = R"(
            int
            putstr(char *s)
            {
                while(*s)
                putchar(*s++);
            }

            int
            fac(int n)
            {
                if (n == 0)
                return 1;
                else
                return n*fac(n-1);
            }

            int
            putn(int n)
            {
                if (9 < n)
                putn(n / 10);
                putchar((n%10) + '0');
            }

            int
            facpr(int n)
            {
                putstr("factorial ");
                putn(n);
                putstr(" = ");
                putn(fac(n));
                putstr("\n");
            }

            int
            main()
            {
                int i;
                i = 0;
                while(i < 10)
                facpr(i++);
                return 0;
            }
)";

    ASTParser parser;
    OCLexer lexer(fact);
    if (!parser.parse(lexer)) {
        const QString& d = lexer.getData();
        int endLine = d.indexOf('\n', lexer.pos());
        endLine = endLine < 0 ? lexer.size() : endLine;
        int begLine = d.lastIndexOf('\n', lexer.pos());
        begLine = begLine < 0 ? 0 : begLine + 1;
        QString context = d.mid(begLine, lexer.pos() - begLine) + " <<<HERE>>> " + d.mid(lexer.pos(), endLine - lexer.pos());
        qDebug() << parser.errorMessage() << context;
    } else {
        qDebug() << parser.result().value<sp_Node>()->toString();
    }
}
