------------------------------------------------------------------------------------
-- A subset C parser, see also README
------------------------------------------------------------------------------------

%parser OCParser

-- operators
%token PLUS "+" MINUS "-" MUL "*" DIV "/" MOD "%" ASSIGN "="
%token LT "<" NOT "!" EQ "==" NEQ "!=" INC "++" DEC "--"

-- keywords
%token INT "int" CHAR "char"
%token IF "if" WHILE "while" DO "do" RETURN "return" ELSE "else"

-- brackets
%token LP "(" RP ")" LC "{" RC "}" LB "[" RB "]"

%token SC ";" COMMA ","

%token IVAL "int value"
%token CVAL "char value"
%token SVAL "string value"
%token IDENT "ident"

%start program

/:

#include <QList>
#include <QStringList>
#include <QSharedPointer>
struct Node;
typedef QSharedPointer<Node> sp_Node;
struct Node {
    Node() { }
    Node(const QString& t) : type(t) { }
    QList<sp_Node> children;
    QString type;
    QString toString(int depth = 0) const {
        QStringList l;
        QString offset = "";
        for (int i = 0; i < depth; ++i) offset+= " ";
        for (const sp_Node& sp_c : children) {
            l.append(sp_c->toString(depth + 4));
        }
        return l.length() ? type + ", children:\n" + offset + l.join("\n" + offset) : type;
    }
};
Q_DECLARE_METATYPE(sp_Node)

class OCLexer : public $lexer
{

public:

    OCLexer(const QString &string) : $lexer(string)
    {
          addToken(Token::PLUS); addToken(Token::MINUS);
          addToken(Token::MUL); addToken(Token::DIV);
          addToken(Token::MOD); addToken(Token::ASSIGN);
          addToken(Token::LT); addToken(Token::NOT);
          addToken(Token::LP); addToken(Token::RP);
          addToken(Token::LC); addToken(Token::RC);
          addToken(Token::LB); addToken(Token::RB);
          addToken(Token::SC);
          addToken(Token::EQ); addToken(Token::NEQ);
          addToken(Token::INC); addToken(Token::DEC);
          enableNumberToken(Token::IVAL, false);
          enableStringToken(Token::SVAL, '"');
          enableStringToken(Token::CVAL, '\'');
          enableIdentifierToken(Token::IDENT);
          addToken(Token::INT, "int");
          addToken(Token::CHAR, "char");
          addToken(Token::IF, "if");
          addToken(Token::ELSE, "else");
          addToken(Token::WHILE, "while");
          addToken(Token::DO, "do");
          addToken(Token::RETURN, "return");

    }
};

class $parser : public $grammar
{
protected:
    virtual void switchRule(int r, $lexer &lexer);
};

class ASTParser : public $parser
{
protected:
    virtual void switchRule(int r, $lexer &lexer);
};

:/

/.

void ASTParser::switchRule(int r, $lexer &lexer) {
    int id = qMetaTypeId<sp_Node>();
    const char * name = spell[rule_info[rule_index[r]]];
    sp_Node n(new Node(name));
    for (int i = 1; i <= rhs[r]; ++i) {
        if (symbol(i).userType() == id) {
            n->children.append(symbol(i).value<sp_Node>());
        } else {
            n->children.append(sp_Node(new Node(symbol(i).toString())));
        }
    }
    symbol(1) = QVariant(QVariant::fromValue<sp_Node>(n));
    $parser::switchRule(r,lexer);
}

./

/r -- begin rules

program ::= decls; /{ setResult(symbol(1)); }/

decls ::= decl;
decls ::= decls decl;

decl ::= vardecl | funcdecl;

vardecl ::= type IDENT SC;
vardecl ::= type IDENT LB IVAL RB;

funcdecl ::= type IDENT LP args RP LC body RC;

args ::= ;
args ::= arg;
args ::= args COMMA arg;

arg ::= type IDENT;

body ::= vardecls stmts;

vardecls ::= ;
vardecls ::= vardecl;
vardecls ::= vardecls vardecl;

stmts ::= ;
stmts ::= stmt;
stmts ::= stmts stmt;
curlystmts ::= LC stmts RC;

stmt ::= ifstmt | whilestmt | dowhilestmt | exprstmt | returnstmt | curlystmts | SC;

exprstmt ::= expr SC;

ifstmt ::= IF LP expr RP stmt;
ifstmt ::= IF LP expr RP stmt ELSE stmt;

whilestmt ::= WHILE LP expr RP stmt;

returnstmt ::= RETURN expr SC;

dowhilestmt ::= DO stmt WHILE LP expr RP SC;

expr ::= binopexpr | unopexpr | bexpr | pexpr | mpexpr | incdecexpr | IDENT | IVAL | CVAL | SVAL;

exprs ::= ;
exprs ::= expr;
exprs ::= exprs expr;

binopexpr ::= expr binop expr;
unopexpr ::= unop expr;
incdecexpr ::= expr incdec;
bexpr ::= expr LB expr RB;
pexpr ::= LP expr RP;
mpexpr ::= expr LP exprs RP;

binop ::= PLUS | MINUS | MUL | DIV | MOD | ASSIGN | LT | EQ | NEQ;

incdec ::= INC | DEC;

unop ::= NOT | MINUS | MUL | incdec;

type ::= inttype | chartype;

inttype ::= INT stars;
chartype ::= CHAR stars;

stars ::= ;
stars ::= MUL;
stars ::= stars MUL;

/e -- end rules
