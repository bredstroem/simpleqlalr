QT       += core
QT       -= gui

TARGET = oc
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

SIMPLEQLALR_SOURCE = oc.g
SIMPLEQLALR_RESULTS = ocparser.cpp ocparser.h

simpleqlalr.commands = ~/bin/simpleqlalr ${QMAKE_FILE_IN}
simpleqlalr.input = SIMPLEQLALR_SOURCE
simpleqlalr.name = simpleqlalr
simpleqlalr.output = $$SIMPLEQLALR_RESULTS
simpleqlalr.variable_out = SOURCES
simpleqlalr.CONFIG += target_predeps
QMAKE_EXTRA_COMPILERS += simpleqlalr

OTHER_FILES += $$SIMPLEQLALR_SOURCE \
    README
