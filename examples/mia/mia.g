%parser MiaGrammar

%token LC "{" RC "}" LS "[" RS "]" LP "(" RP ")"
%token COLON ":" SEMICOLON ";" COMMA "," DOT "."
%token PLUS  "+" MINUS "-" MULT "*" DIV "/"
%token ASSIGN "=" EQUAL "==" LT "<" LTEQ "<=" GT ">" GTEQ ">="
%token WHERE "|" NAMESPACE "::"

%token FALSE "false"
%token TRUE "true"

%token INT    "integer"
%token REAL   "real"
%token STRING "string"
%token NAME   "name"

%start Root

/:
#include <QVector>
#include <QSharedPointer>
#include "value.h"
#include "corevalues.h"
#include "context.h"

class $parser : public $grammar
{

protected:
    void setContext(Context* ctx) { m_ctx = ctx; }
    virtual bool switchRule(int r, $lexer& lexer, int token);
    ValueP &value(int index) { return m_stack[stackOffset()+index]; }
    QVector<ValueP>& stack() { return m_stack; }
    void stackReset() { m_stack.clear(); }
    void stackSizeUpdate(int newSize) { m_stack.resize(newSize); }

private:
    Context*        m_ctx;
    QVector<ValueP> m_stack;
};

:/

/.
#include "value.h"
./

-- code inserted before the rule switching
/b
Context& ctx = *m_ctx;
e/

-- code inserted after the rule switching
/a
if (ctx.hasErr()) {
    setErrorMessage(QString("error in rule=%0: %1").arg(r).arg(spell[rule_info[rule_index[r]]]), lexer);
}
return !ctx.hasErr();
e/

/R -- begin rules

Root            ::= Model ;
    /{ /* context.saveModel() if not already saved! */ }/
Root            ::= Sep Model ;
    /{ /* context.saveModel() if not already saved! */ }/
Root            ::= Model Sep;
    /{ /* context.saveModel() if not already saved! */ }/
Root            ::= Model Sep Root ;
    /{ /* context.saveModel() if not already saved! */ }/

Model           ::= ModelHeader PropertyList RC ;
    /{ /* Nothing, this model is already setup (nothing after '}') */ }/
Model           ::= ModelHeader PropertyList RC Expr ;
    /{ /* model = context.topModel(); model.assign(value(3)); */ }/
Model           ::= ModelHeader PropertyList RC BooleanExpr ;
    /{ /* model = context.topModel(); model.assign(value(3)); */ }/
Model           ::= Name Label ASSIGN Value;
    /{ /* model = context.getModel(value(0), ok); if (ok) context.pushModel(model); model.assign(value(3)); model.setLabel(value(1)); */ }/

ModelHeader     ::= Name Label LC | Name Label ASSIGN LC ;
    /{ /* model = context.getModel(value(0), ok); if (ok) context.pushModel(model); model.setLabel(value(1)); */ }/
ModelHeader     ::= Name LC ;
    /{ /* model = context.getModel(value(0), ok); if (ok) context.pushModel(model); model.setLabel(value(1)); */ }/

Name            ::= NAME ;
Name            ::= NAME NAMESPACE NAME ;
    /{ value(0)->append(value(2), ctx); }/

NEPropertyList    ::= Property ;
NEPropertyList    ::= Property Sep ;
NEPropertyList    ::= Property Sep NEPropertyList ;
PropertyList    ::= ;
PropertyList    ::= NEPropertyList;
PropertyList    ::= NEPropertyList WHERE BooleanExpr;
    /{ /* model = context.topModel(); model.setProperty("where", value(2), ok); */ }/

Property        ::= NAME COLON Value ;
    /{ /* model = context.topModel(); model.setPropery(value(0), value(2), ok); */ }/

Value           ::= Model ;
Value           ::= List ;
Value           ::= Expr ;
Value           ::= TRUE ;
Value           ::= FALSE ;
Value           ::= BooleanExpr;
Value           ::= STRING ;

List            ::= LS ListItems RS ;
    /{ value(0) = value(1); }/
ListItems       ::= ;
    /{  value(0) = ListValue::factory(ctx);  }/
ListItems       ::= Value ;
    /{  value(0) = ListValue::factory(value(0), ctx); }/
ListItems       ::= ListItems Sep Value ; -- 21 --
    /{  value(0)->append(value(2), ctx); }/

RefElement      ::= Label ;
    /{ value(0) = ReferenceValue::factory(value(0)->evalString(ctx), ctx); }/
RefElement      ::= Label LS Expr RS;
    /{ value(0) = ReferenceValue::factory(value(0)->evalString(ctx), value(2), ctx); }/
Reference       ::= RefElement;
Reference       ::= Reference DOT RefElement;
    /{ value(0)->append(value(2).staticCast<ReferenceValue>(), ctx); }/

ExprValue       ::= Reference;
ExprValue       ::= INT;
ExprValue       ::= REAL;
ExprValue       ::= LP Expr RP ;
    /{ value(0) = value(1); }/
ExprValue       ::= PLUS ExprValue ;
    /{ value(0) = value(1); }/
ExprValue       ::= MINUS ExprValue ;
    /{ value(0) = value(1); value(0)->negate(ctx); }/

ExprMult        ::= ExprValue ;
ExprMult        ::= ExprMult MULT ExprValue ;
    /{ value(0) = value(0)->product(value(2), ctx); }/
ExprMult        ::= ExprMult DIV ExprValue ;
    /{ value(0) = value(0)->quotient(value(2), ctx); }/

Expr            ::= ExprMult ;

Expr            ::= Expr PLUS ExprMult ;
    /{ value(0) = value(0)->sum(value(2), ctx); }/
Expr            ::= Expr MINUS ExprMult ;
    /{ value(0) = value(0)->difference(value(2), ctx); }/

BooleanExpr     ::= Expr ComparisonOp Expr ;
    /{ value(0) = BooleanExprValue::factory(value(0), value(2), value(1)->evalInt(ctx), ctx); }/
ComparisonOp    ::= LT | GT | LTEQ | GTEQ ;
    /{ value(0) = IntValue::factory(token, ctx); }/

Label           ::= NAME ;
    /{ /* The label could maybe be something else too than just ID names? */ }/
Sep             ::= SEMICOLON | COMMA ;

/e -- end rules
