#include "corevalues.h"
#include "context.h"

using namespace milklab::mia;

//-----------------------------------------------------------------------------
// BooleanValue
//-----------------------------------------------------------------------------

ValueP BooleanValue::factory(bool val)
{
    BooleanValue* v = new BooleanValue;
    v->value = val;
    return ValueP(v);
}

//-----------------------------------------------------------------------------
// IntValue
//-----------------------------------------------------------------------------

ValueP IntValue::factory(int val)
{
    IntValue* v = new IntValue;
    v->value = val;
    return ValueP(v);
}

//-----------------------------------------------------------------------------
// RealValue
//-----------------------------------------------------------------------------

ValueP RealValue::factory(double val)
{
    RealValue* v = new RealValue;
    v->value = val;
    return ValueP(v);
}

//-----------------------------------------------------------------------------
// StringValue
//-----------------------------------------------------------------------------

ValueP StringValue::factory(QString val)
{
    StringValue* v = new StringValue;
    v->value = val;
    return ValueP(v);
}

void StringValue::multiply(const Value &other, Context& ctx)
{
    QString v = value;
    int i = other.evalInt(ctx);
    while((!ctx.hasError()) && i-- >= 0) value += v;
}

//-----------------------------------------------------------------------------
// NameValue
//-----------------------------------------------------------------------------

ValueP NameValue::factory(QString val)
{
    NameValue* v = new NameValue;
    v->value = val;
    return ValueP(v);
}

void NameValue::append(ValueP newVal, Context& ctx)
{
    if (!moduleName.isEmpty()) {
        setError(ctx);
        return;
    }
    moduleName = value;
    value = newVal->evalString(ctx);
}

QString NameValue::evalString(Context&) const
{
    return moduleName.isEmpty() ? value :
                               QString("%0::%1").arg(moduleName).arg(value);
}


//-----------------------------------------------------------------------------
// ListValue
//-----------------------------------------------------------------------------

ValueP ListValue::factory(Context &)
{
    ListValue* v = new ListValue;
    return ValueP(v);
}

ValueP ListValue::factory(ValueP val, Context& ctx)
{
    ListValue* v = new ListValue;
    v->append(val, ctx);
    return ValueP(v);
}

QString ListValue::toString()
{
    QStringList strings;
    for (ValueP vp : value) { strings.append(vp->toString()); }
    return QString("[%0]").arg(strings.join(","));
}

void ListValue::append(ValueP newVal, Context&)
{
    value.append(newVal);
}


//-----------------------------------------------------------------------------
// ReferenceValue
//-----------------------------------------------------------------------------

ValueP ReferenceValue::factory(Context &)
{
    return ValueP(new ReferenceValue);
}

ValueP ReferenceValue::factory(QString label, Context &)
{
    ReferenceValue* v = new ReferenceValue;
    v->elements.append(RefElement(label));
    return ValueP(v);
}

ValueP ReferenceValue::factory(QString label, ValueP indexExpr, Context &)
{
    ReferenceValue* v = new ReferenceValue;
    v->elements.append(RefElement(label, indexExpr));
    return ValueP(v);
}

void ReferenceValue::append(ValueP other, Context& ctx)
{
    switch (other->valueType()) {
    case Type::Reference:
        elements.append(other.staticCast<ReferenceValue>()->elements);
        break;
    case Type::String:
    case Type::Name:
        elements.append(RefElement(other->evalString(ctx)));
        break;
    default:
        setError(ctx);
    }
}

QString ReferenceValue::toString()
{
    QStringList strings;
    for (RefElement& vp : elements) { strings.append(vp.label); }
    return QString("[%0]").arg(strings.join(","));
}


//-----------------------------------------------------------------------------
// BooleanExprValue
//-----------------------------------------------------------------------------

ValueP BooleanExprValue::factory(ValueP lexpr, ValueP rexpr, int op, Context &)
{
    BooleanExprValue* v = new BooleanExprValue;
    v->lexpr = lexpr;
    v->rexpr = rexpr;
    v->op = op;
    return ValueP(v);
}

QString BooleanExprValue::toString()
{
    return QString("%0 %1 %2").
            arg(lexpr->toString()).
            arg(op).
            arg(rexpr->toString());
}


//-----------------------------------------------------------------------------
// ModelValue
//-----------------------------------------------------------------------------

void ModelValue::setProperty(const QString &name, ValueP value, Context& ctx)
{
    Q_UNUSED(name); Q_UNUSED(value);
    setError(ctx);
}

ValueP ModelValue::getProperty(const QString &name) const
{
    Q_UNUSED(name);
    return ValueP();
}
