#ifndef MODULE_H
#define MODULE_H
#include <QList>
#include <QStringList>
#include "value.h"

namespace milklab {
namespace mia {

class Module;
typedef QSharedPointer<Module> ModuleP;

class Module
{
public:
    virtual const char*           name()             const = 0;
    virtual QList<ValueP>         prototypes()       const = 0;
    virtual QStringList           requiredModules()  const { return QStringList(); }
};

}} //

#endif // MODULE_H
