#ifndef COREVALUES_H
#define COREVALUES_H
#include "value.h"

namespace milklab {
namespace mia {

class BooleanValue : public Value {
public:
    static ValueP factory(bool val);
    static ValueP factory(bool val, Context&) { return factory(val); }
    ValueP clone()          const { return ValueP(new BooleanValue(*this)); }
    Type valueType()        const { return Type::Boolean; }
    QString toString()            { return QString("%0").arg(value); }
    void negate(Context&)         { value = !value; }
    bool value;
};

class IntValue : public Value {
public:
    static ValueP factory(int val);
    static ValueP factory(int val, Context&) { return factory(val); }
    ValueP clone()          const { return ValueP(new IntValue(*this)); }
    Type valueType()        const { return Type::Int; }
    QString toString()            { return QString("%0").arg(value); }

    void negate(Context&)                            { value = -value;  }
    void add(const Value& other, Context& ctx)       { value += other.evalInt(ctx); }
    void subtract(const Value& other, Context& ctx)  { value -= other.evalInt(ctx); }
    void multiply(const Value& other, Context& ctx)  { value *= other.evalInt(ctx); }
    void divide(const Value& other, Context& ctx)    { value /= other.evalInt(ctx); }

    int evalInt(Context&)            const  { return value; }
    double evalReal(Context&)        const  { return value; }
    QString evalString(Context&)     const  { return QString("%0").arg(value); }
    int value;
};

class RealValue : public Value {
public:
    static ValueP factory(double val);
    static ValueP factory(double val, Context&) { return factory(val); }
    ValueP clone()          const { return ValueP(new RealValue(*this)); }
    Type valueType()        const { return Type::Real; }
    QString toString()            { return QString("%0").arg(value); }

    void negate(Context&)                            { value = -value;   }
    void add(const Value& other, Context& ctx)       { value += other.evalReal(ctx); }
    void subtract(const Value& other, Context& ctx)  { value -= other.evalReal(ctx); }
    void multiply(const Value& other, Context& ctx)  { value *= other.evalReal(ctx); }
    void divide(const Value& other, Context& ctx)    { value /= other.evalReal(ctx); }

    int evalInt(Context& ctx)        const  { int cast = value; if(value != double(cast)) setError(ctx); return cast; }
    double evalReal(Context&)        const  { return value; }
    QString evalString(Context&)     const  { return QString("%0").arg(value); }
    double value;
};

class StringValue : public Value {
public:
    static ValueP factory(QString val);
    static ValueP factory(QString val, Context&) { return factory(val); }
    ValueP clone()          const { return ValueP(new StringValue(*this)); }
    Type valueType()        const {return Type::String; }
    QString toString()            { return QString("%0").arg(value); }

    void add(const Value& other, Context& ctx)       { value += other.evalString(ctx); }
    void multiply(const Value& other, Context& ctx);

    int evalInt(Context& ctx)        const  { bool ok; int v = value.toInt(&ok); if(!ok) setError(ctx); return v;  }
    double evalReal(Context& ctx)    const  { bool ok; double v = value.toDouble(&ok); if(!ok) setError(ctx); return v;  }
    QString evalString(Context&)     const  { return QString("%0").arg(value); }
    QString value;
};

class NameValue : public Value {
public:
    static ValueP factory(QString val);
    static ValueP factory(QString val, Context&)    { return factory(val); }
    ValueP clone()          const { return ValueP(new NameValue(*this)); }
    Type valueType()        const {return Type::Name; }
    QString toString()            { return QString("%0").arg(value); }

    void append(ValueP newVal, Context& ctx);

    QString evalString(Context& ctx) const;
    QString moduleName;
    QString value;
};

class ListValue : public Value {
public:
    static ValueP factory(Context& ctx);
    static ValueP factory(ValueP val, Context &ctx);
    ValueP clone()          const { return ValueP(new ListValue(*this)); }
    Type valueType()        const {return Type::List; }
    QString toString();

    void append(ValueP newVal, Context& ctx);

    QList<ValueP> value;
};

class ExprValue : public Value {
public:
    // TODO...
    static ValueP factory(QString expr, Context&) { ExprValue* v = new ExprValue; v->value = expr; return ValueP(v); }
    ValueP clone() const { return ValueP(new ExprValue(*this)); }
    QString toString() { return QString("%0").arg(value); }
    Type valueType()        const {return Type::Expr; }
    QString value;
};

class BooleanExprValue : public Value {
public:
    static ValueP factory(ValueP lexpr, ValueP rexpr, int op, Context& ctx);
    Type valueType()        const {return Type::BoolExpr; }
    ValueP clone()          const { return ValueP(new BooleanExprValue(*this)); }
    QString toString();

    ValueP lexpr;
    ValueP rexpr;
    int op;
};

class ReferenceValue : public Value {
    struct RefElement {
        RefElement(QString l, ValueP ie) : label(l), indexExpr(ie) { }
        RefElement(QString l) : label(l) { }
        QString label;
        ValueP indexExpr;
    };

public:
    ReferenceValue() : coeff(1.0) { }

    static ValueP factory(Context& ctx);
    static ValueP factory(QString label, Context& ctx);
    static ValueP factory(QString label, ValueP indexExpr, Context& ctx);
    ValueP clone()          const { return ValueP(new ReferenceValue(*this)); }
    Type valueType()        const {return Type::Reference; }
    QString toString();

    void negate(Context&)                           { coeff = -coeff;   }

    //void add(const Value& other, Context& ctx)       { coeff += other.evalReal(ok); }
    //void subtract(const Value& other, Context& ctx)  { coeff -= other.evalReal(ok); }

    void multiply(const Value& other, Context& ctx)  { coeff *= other.evalReal(ctx); }
    void divide(const Value& other, Context& ctx)    { coeff /= other.evalReal(ctx); }

    void append(ValueP other, Context& ctx);

    QList<RefElement> elements;

    double coeff;
};

class ModelValue : public Value {
public:
    ModelValue() { }
    virtual Type valueType()        const {return Type::Model; }

    QString label() const { return m_label; }

    virtual void   setProperty(const QString& name, ValueP value, Context& ctx);
    virtual ValueP getProperty(const QString& name) const;

private:
    QString m_label;

};
typedef QSharedPointer<ModelValue> ModelValueP;

}} // namespace milklab::mia


#endif // COREVALUES_H
