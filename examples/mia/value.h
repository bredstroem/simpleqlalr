#ifndef VALUE_H
#define VALUE_H
#include <QSharedPointer>
#include <QString>
#include <QStringList>
#include <QList>

namespace milklab {
namespace mia {

class Context;
class Value;
typedef QSharedPointer<Value> ValueP;

class Value {

public:
    enum class Type {
        Null      = 0,
        Boolean   = 1,
        Int       = 2,
        Real      = 3,
        String    = 4,
        Name      = 100,
        List      = 101,
        Table     = 102,
        Reference = 103,
        Expr      = 201,
        BoolExpr  = 202,
        Model     = 1000,
        Generic = 0x7FFFFFFF
    };

public:
    virtual ~Value() { }

    // Minimum implementation requirements
    virtual QString toString()                    = 0;
    virtual ValueP clone()                  const = 0;
    virtual Type valueType()                const { return Type::Generic; }

    // Compare types with respect to casting/binary ops (the superior type should decide if
    // what is possible)
    bool isSuperiorType(const Value& other) const { return int(valueType()) > int(other.valueType()); }
    bool isEqualType(const Value& other)    const { return int(valueType()) == int(other.valueType()); }
    bool isInferiorType(const Value& other) const { return int(valueType()) < int(other.valueType()); }

    // Implement to enable arithmetic operations on type
    virtual void invert(Context& ctx)                  { setError(ctx); }
    virtual void negate(Context& ctx)                  { setError(ctx); }
    virtual void add(const Value&, Context& ctx)       { setError(ctx); }
    virtual void subtract(const Value&, Context& ctx)  { setError(ctx); }
    virtual void multiply(const Value&, Context& ctx)  { setError(ctx); }
    virtual void divide(const Value&, Context& ctx)    { setError(ctx); }

    // Alternative API for arithmetic operations
    void add(const ValueP&v, Context& ctx)             { add(*v.data(), ctx); }
    void subtract(const ValueP&v, Context& ctx)        { subtract(*v.data(), ctx); }
    void multiply(const ValueP&v, Context& ctx)        { multiply(*v.data(), ctx); }
    void divide(const ValueP&v, Context& ctx)          { divide(*v.data(), ctx); }

    // Default implementations for arithmetic operations with cloned result
    virtual ValueP sum(const Value& v, Context& ctx)       const;
    virtual ValueP difference(const Value&v, Context& ctx) const;
    virtual ValueP product(const Value&v, Context& ctx)    const;
    virtual ValueP quotient(const Value&v, Context& ctx)   const;

    // Alternative API
    ValueP sum(const ValueP&v, Context& ctx)              const  { return sum(*v.data(), ctx); }
    ValueP difference(const ValueP&v, Context& ctx)       const  { return difference(*v.data(), ctx); }
    ValueP product(const ValueP&v, Context& ctx)          const  { return product(*v.data(), ctx); }
    ValueP quotient(const ValueP&v, Context& ctx)         const  { return quotient(*v.data(), ctx); }

    virtual void append(ValueP, Context& ctx) { setError(ctx); }

    // Evaluation methods: try to "cast" or calculate a representation of a core value type.
    virtual bool evalBoolean(Context& ctx)   const { setError(ctx); return false; }
    virtual int evalInt(Context& ctx)        const { setError(ctx); return 0; }
    virtual double evalReal(Context& ctx)    const { setError(ctx); return 0; }
    virtual QString evalString(Context& ctx) const { setError(ctx); return ""; }

protected:
    void setError(Context& ctx, const QString& msg) const;
    void setError(Context& ctx) const { setError(ctx, "error"); }
};


}} // namespace milklab::mia

#endif // VALUE_H
