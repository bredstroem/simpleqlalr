#include "value.h"
#include "context.h"

using namespace milklab::mia;

ValueP Value::sum(const Value &v, Context& ctx) const  {
    if (isInferiorType(v)) {
        ValueP r = v.clone();
        r->add(*this, ctx);
        return r;
    } else {
        ValueP r = this->clone();
        r->add(v, ctx);
        return r;
    }
}

ValueP Value::difference(const Value &v, Context& ctx) const  {
    if (isInferiorType(v)) {
        ValueP r = v.clone();
        r->subtract(*this, ctx);
        if (!ctx.hasError())
            r->negate(ctx);
        return r;
    } else {
        ValueP r = this->clone();
        r->subtract(v, ctx);
        return r;
    }
}

ValueP Value::product(const Value &v, Context& ctx) const  {
    if (isInferiorType(v)) {
        ValueP r = v.clone();
        r->multiply(*this, ctx);
        return r;
    } else {
        ValueP r = this->clone();
        r->multiply(v, ctx);
        return r;
    }
}

ValueP Value::quotient(const Value &v, Context& ctx) const  {
    if (isInferiorType(v)) {
        ValueP r = v.clone();
        r->divide(*this, ctx);
        if (!ctx.hasError())
            r->invert(ctx);
        return r;
    } else {
        ValueP r = this->clone();
        r->divide(v, ctx);
        return r;
    }
}

void Value::setError(Context &ctx, const QString &msg) const
{
    ctx.setError(msg);
}
