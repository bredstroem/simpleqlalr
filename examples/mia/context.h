#ifndef CONTEXT_H
#define CONTEXT_H
#include <QList>
#include <QMap>
#include <QStack>
#include "value.h"
#include "corevalues.h"
#include "module.h"

namespace milklab {
namespace mia {

class BuiltinModule : public Module {
public:
    const char*           name()       const { return "builtin"; }
    // options, license, load, unload, modules ...
    virtual QList<ValueP> prototypes() const { return { }; }
};

class Context
{
public:
    Context() { }
    void registerModule(const Module& module);
    void includeModule(const QString& name);
    void excludeModule(const QString& name);

    ModelValueP getModelValue(QString& name);
    ModelValueP getModelValue(ValueP nameValue);

    void        pushModel(ModelValueP model) { m_modelStack.push(model); }
    ModelValueP topModel()  const            { return m_modelStack.isEmpty() ? ModelValueP() : m_modelStack.top(); }
    ModelValueP popTopModel()                { return m_modelStack.isEmpty() ? ModelValueP() : m_modelStack.pop(); }
    void        saveAndPopTopModel()         { ModelValueP m = m_modelStack.pop(); m_instanceMap[m->label()] = m; }

    void setError(const QString& msg)        { m_hasError = true; m_errMsg = msg; }
    void clearError()                        { m_hasError = false; m_errMsg.clear(); }
    bool hasError() const                    { return m_hasError; }
    QString errorMessage() const             { return m_errMsg;   }

private:
    QMap<QString, ModuleP>      m_moduleMap;
    QMap<QString, ValueP>       m_rootPrototypeMap;
    QMap<QString, ModelValueP>  m_instanceMap;

    QStack<ModelValueP>         m_modelStack;
    bool m_hasError;
    QString m_errMsg;
};

}} // namespace milklab::mia

#endif // CONTEXT_H
