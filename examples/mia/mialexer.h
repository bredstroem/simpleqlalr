#ifndef MIALEXER_H
#define MIALEXER_H
#include <QTextStream>
#include <QString>
#include <QChar>
#include "value.h"
#include "miagrammar.h"

namespace milklab {
namespace mia {

class MiaLexer : public MiaGrammarLexerBase {

public:
    MiaLexer(QTextStream& input) :
        m_input(input),
        m_previousToken(-1),
        m_pos(0), m_line(0) { }

    ValueP getValue() const { return m_value; }

    // MiaGrammarLexerBase interface
public:
    int lex();

    int lineNumber() const { return m_line; }
    int pos()        const { return m_pos; }

    int token()      const { return m_previousToken; }

    QString context() const { return getValue()->toString(); }

private:
    int internalLex();
    char nextChar(bool skipWhiteSpace = false);

    QTextStream& m_input;
    int m_previousToken;
    QString m_buff;
    QChar m_current;
    int m_pos;
    int m_line;
    // QStack<char> m_objectStack;
    ValueP m_value;
};

}
}

#endif // MIALEXER_H
