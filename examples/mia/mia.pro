QT       += core
QT       -= gui

TARGET = mia
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    miagrammar.cpp \
    mialexer.cpp \
    value.cpp \
    corevalues.cpp \
    context.cpp \
    miaparser.cpp \
    module.cpp

#SIMPLEQLALR_SOURCE = mia.g
#SIMPLEQLALR_RESULTS = miagrammar.cpp miagrammar.h
#simpleqlalr.commands = ~/bin/simpleqlalr ${QMAKE_FILE_IN}
#simpleqlalr.input = SIMPLEQLALR_SOURCE
#simpleqlalr.name = simpleqlalr
#simpleqlalr.output = $$SIMPLEQLALR_RESULTS
#simpleqlalr.variable_out = SOURCES
#simpleqlalr.CONFIG += target_predeps
#QMAKE_EXTRA_COMPILERS += simpleqlalr

OTHER_FILES += mia.g

HEADERS += \
    miagrammar.h \
    mialexer.h \
    value.h \
    corevalues.h \
    context.h \
    miaparser.h \
    module.h
