#include <QCoreApplication>
#include <QDebug>
#include <QTextStream>
#include "miaparser.h"

using namespace milklab;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QStringList strs = {
        R"(sometype x {data: (1.0+5.5)/2*5.2 + 23, somelist: [1,2,3+5] } )",
        R"(var x = 5;)",
        R"(var y = { integer:true; } 5;)",
        R"(set I = [1,2,3,4,5];)",
        "range J { min: I[0], max: I[I.length], step: 1 }",
        "set J { j : I, s : S, k : K | 2.4*x[4+54] >= 5 }",
        "builtin::root { }"
    };
    mia::MiaParser parser;
    mia::Context ctx;

    for (QString s : strs) {
        QTextStream stream(&s);
        mia::MiaLexer lexer(stream);
        if (!parser.parseIntoContext(lexer, ctx)) {
            qDebug() << s << parser.errorMessage() << lexer.context();
        } else {
            qDebug() << s << ":YES";
        }

    }
}
