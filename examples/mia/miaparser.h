#ifndef MIAPARSER_H
#define MIAPARSER_H
#include "miagrammar.h"
#include "mialexer.h"
#include "value.h"
#include "context.h"

namespace milklab {
namespace mia {

class MiaParser : public MiaGrammar
{
    // MiaGrammarBase interface
public:
    bool parseIntoContext(MiaGrammarLexerBase &lex, Context& ctx)
    {
        MiaGrammar::setContext(&ctx);
        return parse(lex);
    }

    bool parse(MiaGrammarLexerBase &lex)
    {
        m_lexer = static_cast<MiaLexer*>(&lex);
        return MiaGrammar::parse(lex);
    }

    // MiaGrammarBase interface
protected:
    void stackValueUpdate(int pos)
    {
        stack()[pos] = m_lexer->getValue();
    }

private:
    MiaLexer* m_lexer;
};


}} // milklab::mia

#endif // MIAPARSER_H
