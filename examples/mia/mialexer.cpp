#include "mialexer.h"
#include "miagrammar.h"

using namespace milklab::mia;


int MiaLexer::lex()
{
    m_previousToken = internalLex();
    return m_previousToken;
}

int MiaLexer::internalLex()
{
    m_buff.clear();
    char lc = m_current.isNull() || m_current.isSpace() ? nextChar(true) : m_current.toLatin1();
    if (!lc)
        return Token::EOF_SYMBOL;

    bool hasDot = false;
    if (lc == '.') {
        // special case because a float could begin with '.' without a digit
        // in front.
        char lc2 = nextChar();
        if (lc2 >= '0' && lc2 <= '9') {
            lc = lc2;
            m_buff.append("0.");
            hasDot = true;
        }
        else {
            return Token::DOT;
        }
    }

    if (lc >= '0' && lc <= '9') {
        do {
            m_buff.append(lc);
            lc = nextChar();
        } while ((lc >= '0' && lc <= '9'));
        if (hasDot || lc == '.' || lc == 'e' || lc == 'E') {
            if (!hasDot && lc == '.') {
                do {
                    m_buff.append(lc);
                    lc = nextChar();
                } while ((lc >= '0' && lc <= '9'));
            }
            if (lc == 'e' || lc == 'E') {
                m_buff.append(lc);
                lc = nextChar();
                if ((lc >= '0' && lc <= '9') || lc == '+' || lc == '-') {
                    do {
                        m_buff.append(lc);
                        lc = nextChar();
                    } while ((lc >= '0' && lc <= '9'));
                }
            }
            bool ok;
            double value = m_buff.toDouble(&ok);
            m_value = RealValue::factory(value);
            return ok ? Token::REAL : -1;
        }
        int value = m_buff.toInt();
        m_value = IntValue::factory(value);
        return Token::INT;
    }
    else if ((lc >= 'A' && lc <= 'Z') || (lc >= 'a' && lc <= 'z')) {
        do {
            m_buff.append(lc);
            lc = nextChar();
        } while ((lc >= 'A' && lc <= 'Z') ||
                 (lc >= 'a' && lc <= 'z') ||
                 (lc >= '0' && lc <= '9') ||
                 lc == '_');
        if (m_buff == "true") return Token::TRUE;
        if (m_buff == "false") return Token::FALSE;
        QString name = m_buff;
        m_value = NameValue::factory(name);
        return Token::NAME;
    }
    else if (lc == '"') {
        bool esc = false;
        QString unicode;
        while (1) {
            lc = nextChar();
            if (!lc) {
                return -1;
            }
            else if (esc) {
                switch (lc) {
                case 'b': m_buff.append(''); break;
                case 'f': m_buff.append("\f"); break;
                case 'n': m_buff.append("\n"); break;
                case 'r': m_buff.append("\r"); break;
                case '\\': m_buff.append("\\"); break;
                case '"': m_buff.append("\""); break;
                case 'u':
                    unicode.clear();
                    for (int i = 0; i < 4 && nextChar(); ++i) {
                        unicode.append(m_current);
                    }
                    if (unicode.length() < 4) return -1;
                    m_buff.append(QChar(unicode.toShort(0, 16)));
                default:
                    return -1;
                }

                esc = false;
            }
            else if (lc == '\\') {
                esc = true;
            }
            else if (lc == '"') {
                m_value = StringValue::factory(m_buff);
                m_current = QChar();
                return Token::STRING;
            }
            else {
                m_buff.append(m_current); // Note: not lc but the QChar
            }
        }
    }
    else {
        // One letter tokens
        m_value = StringValue::factory(QString("Token '%0'").arg(lc));
        char lc2 = nextChar(); // This is ok, we will use it the next time lex is called unless we do it here.
        switch (lc) {
        case '{': return Token::LC;
        case '[': return Token::LS;

        case '}': return Token::RC;
        case ']': return Token::RS;

        case ':':
            {
                if (lc2 == ':') {
                    nextChar();
                    m_value.staticCast<StringValue>()->value = "::";
                    return Token::NAMESPACE;
                }
                return Token::COLON;
            }
        case '(': return Token::LP;
        case ')': return Token::RP;
        case ';': return Token::SEMICOLON;
        case ',': return Token::COMMA;
        case '/':
            {
                if (lc2 == '/') {
                    while((lc != '\n') && lc) lc = nextChar();
                    return lc ? lex() : -1;
                } else if (lc2 == '*') {
                    forever {
                        while((lc != '*') && lc) lc = nextChar();
                        lc2 = nextChar();
                        if (lc2 == '/') {
                            nextChar();
                            return lex();
                        } else if (lc2) {
                            lc = lc2;
                        } else {
                            return -1;
                        }
                    }
                }
                return Token::DIV;
            }
        case '*': return Token::MULT;
        case '-': return Token::MINUS;
        case '+': return Token::PLUS;
        case '.': return Token::DOT;
        case '|': return Token::WHERE;
        case '=':
            {
                if (lc2 == '=') {
                    nextChar(); // don't look at this next time
                    m_value.staticCast<StringValue>()->value = "==";
                    return Token::EQUAL;
                }
                return Token::ASSIGN;
            }
        case '<':
            {
                if (lc2 == '=') {
                    nextChar(); // don't look at this next time
                    m_value.staticCast<StringValue>()->value = "<=";
                    return Token::LTEQ;
                }
                return Token::LT;
            }
        case '>':
            {
                if (lc2 == '=') {
                    nextChar(); // don't look at this next time
                    m_value.staticCast<StringValue>()->value = ">=";
                    return Token::GTEQ;
                }
                return Token::GT;
            }
        }
    }
    return -1;
}

char MiaLexer::nextChar(bool skipWhiteSpace) {
    char lc;
    do {
        if (m_input.atEnd()) {
            m_current = QChar();
            return 0;
        }

        m_input >> m_current;
        lc = m_current.toLatin1();
        if (lc == '\n') {
            m_line++;
            m_pos = 0;
        }
        m_pos++;
    } while (skipWhiteSpace && m_current.isSpace());
    return lc;
}
