/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the utils of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QtCore/QFile>
#include <QtCore/QStringList>
#include <QtCore/QtDebug>
#include <QtCore/QRegExp>
#include <QCommandLineParser>
#include <QCommandLineOption>

#include <cstdlib>

#include "lalr.h"
#include "dotgraph.h"
#include "parsetable.h"
#include "cppgenerator.h"
#include "recognizer.h"

#define QLALR_NO_DEBUG_TABLE
#define QLALR_NO_DEBUG_DOT

int main (int argc, char *argv[])
{
    QCoreApplication app (argc, argv);
    QCoreApplication::setApplicationName("simpleqlalr");
    QCoreApplication::setApplicationVersion(APPLICATION_VERSION);

    bool generate_dot = false;
    bool generate_report = false;
    bool no_lines = false;
    bool debug_info = true;
    bool qt_copyright = false;
    QString file_name = 0;

    QCommandLineParser parser;
    parser.setApplicationDescription("Simple QLALR parser generator");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("filename", "input file name");

    QCommandLineOption verboseOption(QStringList() << "v" << "verbose", "Verbose output");
    parser.addOption(verboseOption);
    QCommandLineOption noLinesOption("no-lines", "Don't include #line");
    parser.addOption(noLinesOption);
    QCommandLineOption dotOption("dot", "Generate dot graph");
    parser.addOption(dotOption);
    QCommandLineOption namespaceOption(QStringList() << "n" << "namespace", "Use a namespace", "name");
    parser.addOption(namespaceOption);
    parser.process(app);

    const QStringList args = parser.positionalArguments();
    if (args.length() == 0) {
        parser.showHelp(1);
    }
    file_name = args.at(0);
    generate_dot = parser.isSet(dotOption);
    no_lines = parser.isSet(noLinesOption);
    generate_report = parser.isSet(verboseOption);

    if (file_name.isEmpty ())
    {
      parser.showHelp(1);
    }

    Grammar grammar;

    if (parser.isSet(namespaceOption)) {
        grammar.ns_name = parser.value(namespaceOption).split("::");
    }

    Recognizer p (&grammar, no_lines);

    if (! p.parse (file_name))
        exit (EXIT_FAILURE);

    if (!grammar.merged_output.isEmpty()) {
        qerr << "Sorry: Merged output is not supported in simpleqlalr" << endl;
        exit (EXIT_FAILURE);
    }

    if (grammar.rules.isEmpty ())
    {
        qerr << "*** Fatal. No rules!" << endl;
        exit (EXIT_FAILURE);
    }

    else if (grammar.start == grammar.names.end ())
    {
        qerr << "*** Fatal. No start symbol!" << endl;
        exit (EXIT_FAILURE);
    }

    grammar.buildExtendedGrammar ();
    grammar.buildRuleMap ();

    Automaton aut (&grammar);
    aut.build ();

    CppGenerator gen (p, grammar, aut, generate_report);
    gen.setDebugInfo (debug_info);
    gen.setCopyright (qt_copyright);
    gen ();

    if (generate_dot)
    {
        DotGraph genDotFile (qout);
        genDotFile (&aut);
    }

    else if (generate_report)
    {
        ParseTable genParseTable (qout);
        genParseTable(&aut);
    }

    return EXIT_SUCCESS;
}

QString Recognizer::expand (const QString &text) const
{
  QString code = text;

  if (_M_grammar->start != _M_grammar->names.end ())
    {
      code = code.replace (QLatin1String("$start_id"), QString::number (std::distance (_M_grammar->names.begin (), _M_grammar->start)));
      code = code.replace (QLatin1String("$start"), *_M_grammar->start);
    }

  code = code.replace (QLatin1String("$table"), _M_grammar->table_name);
  code = code.replace (QLatin1String("$grammar"), _M_grammar->table_name);
  code = code.replace (QLatin1String("$parser"), _M_grammar->parser_name);
  code = code.replace (QLatin1String("$lexer"), _M_grammar->parser_name + "LexerBase");

  if (_M_current_rule != _M_grammar->rules.end ())
    {
      code = code.replace (QLatin1String("$rule_number"), QString::number (std::distance (_M_grammar->rules.begin (), _M_current_rule)));
      code = code.replace (QLatin1String("$rule"), *_M_current_rule->lhs);
    }

  return code;
}
