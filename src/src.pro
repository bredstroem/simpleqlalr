
TEMPLATE = app
QT = core
CONFIG += console c++11
TARGET = simpleqlalr
mac:CONFIG -= app_bundle

VERSION = $$system(git describe --abbrev --tags --dirty)
DEFINES += APPLICATION_VERSION=\"\\\"$$VERSION\\\"\"

SOURCES += compress.cpp \
    cppgenerator.cpp \
    dotgraph.cpp \
    lalr.cpp \
    main.cpp \
    parsetable.cpp \
    recognizer.cpp \
    grammar.cpp

HEADERS += compress.h \
    cppgenerator.h \
    dotgraph.h \
    lalr.h \
    parsetable.h \
    grammar_p.h

OTHER_FILES = lalr.g ../README
